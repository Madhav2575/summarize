import requests
import json

data = {"search_query": "food order using AI"}

response = requests.post('http://127.0.0.1:5000/search', data=json.dumps(data))

fetch = requests.get('http://127.0.0.1:5000/fetch_ideas')
print(fetch.json())
print(response.json())
