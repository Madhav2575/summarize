import flask
from flask import request, Response
import json
import requests
from bs4 import BeautifulSoup
from googlesearch import search
import pickle
import mysql.connector
import pandas as pd

app = flask.Flask(__name__)
app.config['DEBUG'] = True


def summarizer(paras):
    model = pickle.load(open("summarizemodel",'rb'))
    article_text =' '.join(paras)
    result = model(article_text, min_length=60)
    summarized_result = ''.join(result)
    return summarized_result


def save2db(search_query, search_results):
    sql = 'Insert into idea (user_id, idea_statement) values(%s, %s)'
    values = (1, search_query)
    mycursor.execute(sql, values)
    mydb.commit()
    idea_id = mycursor.lastrowid
    search_results = pd.DataFrame(search_results['google_search_results'])
    for i, row in search_results.iterrows():
        sql = 'Insert into summarize (idea_id, url, summarized_text) values(%s, %s, %s)'
        values = (idea_id, row['url'], row['summarized_text'])
        mycursor.execute(sql, values)
        mydb.commit()


@app.route('/', methods = ['GET'])
def home():
    return '<h1>Welcome to Search Summaraization</h1>'


@app.route('/search', methods = ['GET', 'POST'])
def search_summarizer():
    if request.method == 'POST':
        try:
            request_data = json.loads(request.data)
            search_query = request_data['search_query']
            search_results = {}
            search_results['google_search_results'] = []
            for base_site in search(search_query, stop=5):
                response = requests.get(base_site)
                if response.status_code == 200:
                    html = response.content
                    soup = BeautifulSoup(html, "html.parser")
                    text_list = []
                    for item in soup.findAll('p'):
                        text_list.append(item.text)
                    summarized_text_list = summarizer(text_list)
                    search_results['google_search_results'].append({"url": base_site,
                                                                    "summarized_text": summarized_text_list})
                else:
                    search_results['google_search_results'].append({"url": base_site,
                                                                    "summarized_text": ""})
            save2db(search_query, search_results)
            return Response(json.dumps(search_results))
        except Exception as e:
            print(e)
            return Response(status=400)


@app.route('/fetch_ideas', methods=['GET'])
def fetch_ideas():
    sql = 'SELECT ' \
          'i.idea_statement,' \
          ' i.created_date,' \
          ' s.url,' \
          ' s.summarized_text' \
          ' FROM idea i LEFT JOIN summarize s on s.idea_id = i.id WHERE i.user_id = 1 order by i.created_date desc'
    results = pd.read_sql(sql, con=mydb)
    ideas = set(results['idea_statement'])
    response = {}
    response['summarize'] = []
    for idea in ideas:
        idea_results = {}
        idea_results['idea'] = idea
        created_date = results.loc[results['idea_statement'] == idea, 'created_date']\
            .values[0]
        created_date = pd.to_datetime(str(created_date))
        idea_results['created_date'] = created_date.strftime('%Y.%m.%d')
        url = results.loc[results['idea_statement'] == idea, 'url'].values.tolist()
        summarized_text = results.loc[results['idea_statement'] == idea, 'summarized_text']\
            .values.tolist()
        idea_results['idea_results'] = dict(zip(url, summarized_text))
        response['summarize'].append(idea_results)
    return json.dumps(response)


if __name__ == '__main__':
    mydb = mysql.connector.connect(
        host="localhost",
        user="root",
        password="",
        database="wifu"
    )
    mycursor = mydb.cursor()
    
app.run()
